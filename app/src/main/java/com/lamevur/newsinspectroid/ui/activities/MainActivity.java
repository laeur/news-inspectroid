package com.lamevur.newsinspectroid.ui.activities;

import android.content.Intent;
import android.graphics.Rect;
import android.graphics.drawable.ColorDrawable;
import android.support.annotation.NonNull;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.webkit.WebView;
import android.widget.PopupWindow;
import android.widget.ProgressBar;

import com.lamevur.newsinspectroid.R;
import com.lamevur.newsinspectroid.customs.CustomWebChromeClient;
import com.lamevur.newsinspectroid.customs.CustomWebViewClient;


public class MainActivity
        extends AppCompatActivity
        implements View.OnClickListener {

    private String BASE_URL;

    private WebView mNewsViewer          = null;
    private ProgressBar mLoadingProgress = null;
    private PopupWindow mSharePopup      = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // WebView configuration
        BASE_URL = getResources().getString(R.string.BASE_URL);
        mLoadingProgress = (ProgressBar) findViewById(R.id.activitymain_pb_progress);
        mNewsViewer = (WebView) findViewById(R.id.activitymain_wv_site);
        mNewsViewer.setWebViewClient(new CustomWebViewClient());
        mNewsViewer.setWebChromeClient(new CustomWebChromeClient(mLoadingProgress));
        mNewsViewer.getSettings().setJavaScriptEnabled(true);
        mNewsViewer.getSettings().setDomStorageEnabled(true);
        mNewsViewer.loadUrl(BASE_URL);
    }

    public void onClick(View view) {

        if(mNewsViewer == null)
            return;

        switch (view.getId()) {
            case R.id.navigationmenu_btn_back:
                mNewsViewer.goBack();
                break;

            case R.id.navigationmenu_btn_forward:
                mNewsViewer.goForward();
                break;

            case R.id.navigationmenu_btn_share:
                //showPopup(view);
                sharePopupButtonClick();
                break;

            case R.id.navigationmenu_btn_update:
                mNewsViewer.loadUrl(mNewsViewer.getUrl());
                break;

            case R.id.navigationmenu_btn_home:
                mNewsViewer.loadUrl(BASE_URL);
                break;
        }
    }

    public void sharePopupButtonClick() {
        Intent shareIntent = new Intent(Intent.ACTION_SEND);
        shareIntent.putExtra(Intent.EXTRA_TEXT, mNewsViewer.getUrl());
        shareIntent.setType("text/plain");
        startActivity(shareIntent);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mNewsViewer.saveState(outState);
    }

    @Override
    protected void onRestoreInstanceState(@NonNull Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        mNewsViewer.restoreState(savedInstanceState);
    }
}
