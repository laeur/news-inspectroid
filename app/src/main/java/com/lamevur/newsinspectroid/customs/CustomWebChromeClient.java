package com.lamevur.newsinspectroid.customs;

import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.widget.ProgressBar;

public class CustomWebChromeClient extends WebChromeClient {

    private final ProgressBar mProgress;
    private final int PROGRESS_MAX_VALUE = 100;

    public CustomWebChromeClient(ProgressBar progress) {
        this.mProgress = progress;
        this.mProgress.setMax(PROGRESS_MAX_VALUE);
    }

    @Override
    public void onProgressChanged(WebView view, int newProgress) {
        if(mProgress.getVisibility() == View.GONE && newProgress < PROGRESS_MAX_VALUE) {
            mProgress.setVisibility(View.VISIBLE);
        }
        mProgress.setProgress(newProgress);
        if(newProgress == PROGRESS_MAX_VALUE) {
            mProgress.setVisibility(View.GONE);
        }
    }
}
