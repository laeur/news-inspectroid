package com.lamevur.newsinspectroid.customs;

import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.lamevur.newsinspectroid.R;

public class CustomWebViewClient extends WebViewClient {

    @Override
    public boolean shouldOverrideUrlLoading(WebView view, String url) {
        final String BASE_URL = view.getContext().getString(R.string.BASE_URL);
        if(url.startsWith(BASE_URL)) {
            view.loadUrl(url);
            return true;
        } else
            return false;
    }

}
